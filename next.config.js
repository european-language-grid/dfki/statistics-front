const isProd = process.env.NODE_ENV === "production";

module.exports = {
  assetPrefix: isProd ? "/statistics/" : "",
  images: {
    path: isProd ? "/statistics/_next/image" : "/_next/image",
  },
};
