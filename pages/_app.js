import "../styles/globals.css";

import Router, { useRouter } from "next/router";
import { useEffect, useState } from "react";

import Head from "next/head";
import Image from "next/image";

const isProd = process.env.NODE_ENV === "production";
const linkBasePath = isProd ? "/statistics/" : "/";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const start = () => {
      setLoading(true);
    };
    const end = () => {
      setLoading(false);
    };
    Router.events.on("routeChangeStart", start);
    Router.events.on("routeChangeComplete", end);
    Router.events.on("routeChangeError", end);
    return () => {
      Router.events.off("routeChangeStart", start);
      Router.events.off("routeChangeComplete", end);
      Router.events.off("routeChangeError", end);
    };
  }, []);
  return (
    <>
      <Head>
        <title>ELG statistics</title>
        <meta
          name="description"
          content="European Language Grid statistics page"
        />
      </Head>
      {router.asPath !== linkBasePath && (
        <>
          <a href={linkBasePath}>
            <div className={"logo backMenu"}>
              <Image
                src="/icons/arrow_back_black_24dp.svg"
                alt="Back to main page"
                width={72}
                height={72}
              />
            </div>
          </a>
          <hr></hr>
        </>
      )}

      <main className={"main"}>
        {loading ? (
          <h1>
            <center>Loading...</center>
          </h1>
        ) : (
          <Component {...pageProps} />
        )}
      </main>
      <footer className={"footer"}>
        <div>
          <p>European Language Grid statistics page</p>
        </div>
        <div className={"logo"}>
          <Image
            src="/logo/rgb_elg__symbol--colour.svg"
            alt="ELG Logo"
            width={72}
            height={72}
          />
        </div>
      </footer>
    </>
  );
}

export default MyApp;
