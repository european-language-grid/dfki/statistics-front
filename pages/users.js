import { Cell } from "../components/Cell";
import ChartDataLabels from "chartjs-plugin-datalabels";
import Link from "next/link";
import Router from "next/router";

export default function Home({
  usersProvider,
  usersCreation,
  usersTypeCreation,
  topProviders,
  activeProviders,
  connections,
}) {
  return (
    <>
      <div className="title">
        <h1> ELG users statistics</h1>
        <div className="links">
          <div className="link">
            <Link href="#usersProvider">
              <a>Provider users</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#usersCreation">
              <a>Number of users</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#usersTypeCreation">
              <a>Detail number of users</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#connections">
              <a>Unique users per month</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#activeProviders">
              <a>Active providers</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#topProviders">
              <a>Details active providers</a>
            </Link>
          </div>
        </div>
      </div>
      <div>
        <Cell
          pie={true}
          id="usersProvider"
          title="Provider users"
          description=""
          small={true}
          data={usersProvider}
          plugins={[ChartDataLabels]}
          options={{
            animation: false,
          }}
        />
        <Cell
          line={true}
          id="usersCreation"
          title="Number of users in function of the time"
          description="Users correspond to Keycloak accounts in this graph."
          data={usersCreation}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
            animation: false,
          }}
        />

        <Cell
          line={true}
          id="usersTypeCreation"
          title="Detail of the number of users in function of the time"
          description="Users correspond to Keycloak accounts in this graph. Consumers and providers are Keycloak roles."
          data={usersTypeCreation}
          options={{
            animation: false,
          }}
        />
        <Cell
          bar={true}
          id="connections"
          title="Number of unique active users per month"
          description="Active users mean unique accounts that logged in at least one time during the month in this graph."
          data={connections}
          options={{
            animation: false,
          }}
        />
        <Cell
          pie={true}
          id="activeProviders"
          title="Active providers"
          description="Number of providers by the number of resources provided."
          small={true}
          data={activeProviders}
          plugins={[ChartDataLabels]}
          options={{
            animation: false,
          }}
        />
        {/* <Cell
          bar={true}
          chartHeight={400}
          id="topProviders"
          title="Detail of the resources provided by the providers"
          description="Number of resources the providers provided. A resource can be provided by multiple providers, and some resources have no provider."
          tall={true}
          data={topProviders}
          options={{
            maintainAspectRatio: false,
            indexAxis: "y",
            animation: false,
            scales: {
              x: {
                stacked: true,
              },
              y: {
                stacked: true,
              },
            },
          }}
        /> */}
        <Cell
          table={true}
          id="topProviders"
          title="Detail of the resources provided by the providers"
          description="Number of resources the providers provided. A resource can be provided by multiple providers, and some resources have no provider."
          data={topProviders}
        />
      </div>
    </>
  );
}

async function getData(path, params = null) {
  function encode_params(params) {
    if (params == null) {
      return "";
    }
    var result = "?";
    for (var [k, vs] of Object.entries(params)) {
      if (typeof vs[Symbol.iterator] !== "function") {
        vs = [vs];
      }
      vs.forEach(function (v, _) {
        result += `${k}=${v}&`;
      });
    }
    return result.slice(0, -1);
  }
  const dataReq = await fetch(
    `${process.env.FASTAPI_URL}/${path}${encode_params(params)}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
  const data = await dataReq.json();
  return data;
}

export async function getServerSideProps() {
  return {
    props: {
      usersProvider: await getData("users/provider", {}),
      usersCreation: await getData("users/createdTimestamp", {
        cumulate: true,
        nb_of_colors: 1,
      }),
      usersTypeCreation: await getData("users/createdTimestamp", {
        cumulate: true,
        nb_of_colors: 1,
        one_dataset_per_type: true,
      }),
      topProviders: await getData("active-providers", {
        total: true,
      }),
      activeProviders: await getData("active-providers", {
        pie: true,
        total: true,
      }),
      connections: await getData("connections", {}),
    },
  };
}
