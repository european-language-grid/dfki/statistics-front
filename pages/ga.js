import { useEffect, useRef, useState } from "react";

import { Cell } from "../components/Cell";
import ChartDataLabels from "chartjs-plugin-datalabels";
import Link from "next/link";
import Router from "next/router";

const isProd = process.env.NODE_ENV === "production";
const linkBasePath = isProd ? "/statistics/" : "/";

export default function Home({
  fromDateQuery,
  toDateQuery,
  activeUsers,
  usersByMonths,
  pagesByMonths,
  pages,
  // mediums,
  channels,
  sources,
}) {
  const [fromDate, setFromDate] = useState(fromDateQuery);
  const [toDate, setToDate] = useState(toDateQuery);

  function refresh(fromDate, toDate) {
    Router.push({
      pathname: `${linkBasePath}ga`,
      query: { fromDate: encodeURI(fromDate), toDate: encodeURI(toDate) },
    });
  }
  return (
    <>
      <div className="title">
        <h1> ELG analytics statistics</h1>
        <div className="timeIntervalDiv">
          <label for="start">From: </label>
          <input
            type="date"
            id="fromDate"
            name="fromDate"
            value={fromDate}
            min="2020-01-01"
            max="2023-01-01"
            onChange={(event) => setFromDate(event.target.value)}
          ></input>
          <label for="start"> To: </label>
          <input
            type="date"
            id="toDate"
            name="toDate"
            value={toDate}
            min="2020-01-01"
            max="2023-01-01"
            onChange={(event) => setToDate(event.target.value)}
          ></input>
          <button
            className="refreshButton"
            onClick={() => refresh(fromDate, toDate)}
          >
            Refresh
          </button>
        </div>
        <div className="links">
          <div className="link">
            <Link href="#activeUsers">
              <a>Active users</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#usersByMonths">
              <a>Users per month</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#channels">
              <a>Channels</a>
            </Link>
          </div>
          {/* <div className="link">
            <Link href="#mediums">
              <a>Mediums</a>
            </Link>
          </div> */}
          <div className="link">
            <Link href="#sources">
              <a>Sources</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#pagesByMonths">
              <a>Views and average time per month</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#pages">
              <a>Pages</a>
            </Link>
          </div>
        </div>
      </div>
      <div>
        <Cell
          line={true}
          id="activeUsers"
          title="Number of active users in function of the time"
          description="The notion of active user in this graph corresponds to the definition of Google Analytics and does not correspond to a Keycloak account."
          data={activeUsers}
          options={{
            animation: false,
          }}
        />
      </div>
      <div>
        <Cell
          bar={true}
          id="usersByMonths"
          title="Number of users per month"
          description="The notion of user and new user in this graph corresponds to the definition of Google Analytics. The number of regular users is simply the number of users minus the number of new users."
          data={usersByMonths}
          options={{
            animation: false,
            scales: {
              y: {
                stacked: true,
              },
              x: {
                stacked: true,
              },
            },
          }}
        />
      </div>
      <div>
        <Cell
          pie={true}
          id="channels"
          small={true}
          title="Channels of the new users"
          description="Channels in Google Analytics are high-level categories indicating how people found your site."
          data={channels}
          plugins={[ChartDataLabels]}
          options={{
            animation: false,
          }}
        />
      </div>
      {/* <div>
        <Cell
          pie={true}
          id="mediums"
          small={true}
          title="Mediums of the new users"
          description=""
          data={mediums}
          plugins={[ChartDataLabels]}
          options={{
            animation: false,
          }}
        />
      </div> */}
      <div>
        <Cell
          twoColumnsTable={true}
          // chartHeight="30rem"
          id="sources"
          label="Sources"
          title="Sources of the new users"
          description="Detail where people came from according to Google Analytics."
          data={sources}
        />
      </div>
      <div>
        <Cell
          bar={true}
          id="pagesByMonths"
          title="Number of views and average time on pages per month"
          description="Numbers taken from Google Analytics."
          data={pagesByMonths}
          options={{
            animation: false,
            scales: {
              y0: {
                type: "linear",
                display: true,
                position: "left",
              },
              y1: {
                type: "linear",
                display: true,
                position: "right",
                grid: {
                  drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
              },
            },
          }}
        />
      </div>
      <div>
        <Cell
          table={true}
          id="pages"
          title="Views, average time, and exit rate per pages"
          description="Only the 100 most viewed pages are displayed. Numbers taken from Google Analytics."
          data={pages}
          label="path"
        />
      </div>
    </>
  );
}

async function getData(path, params = null) {
  function encode_params(params) {
    if (params === null) {
      return "";
    }
    var result = "?";
    for (var [k, vs] of Object.entries(params)) {
      if (typeof vs[Symbol.iterator] !== "function" || typeof vs === "string") {
        vs = [vs];
      }
      vs.forEach(function (v, _) {
        result += `${k}=${v}&`;
      });
    }
    return result.slice(0, -1);
  }
  const dataReq = await fetch(
    `${process.env.FASTAPI_URL}/${path}${encode_params(params)}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
  const data = await dataReq.json();
  return data;
}

export async function getServerSideProps({ query }) {
  const fromDate = query.fromDate ? query.fromDate : "2020-01-01";
  const toDate = query.toDate ? query.toDate : "2023-01-01";
  return {
    props: {
      fromDateQuery: fromDate,
      toDateQuery: toDate,
      activeUsers: await getData("analytics/users", {
        from_date: fromDate,
        to_date: toDate,
      }),
      usersByMonths: await getData("analytics/users-by-months", {
        from_date: fromDate,
        to_date: toDate,
      }),
      pagesByMonths: await getData("analytics/pages-by-months", {
        from_date: fromDate,
        to_date: toDate,
      }),
      pages: await getData("analytics/pages", {
        from_date: fromDate,
        to_date: toDate,
      }),
      mediums: await getData("analytics/mediums", {
        from_date: fromDate,
        to_date: toDate,
      }),
      channels: await getData("analytics/channels", {
        from_date: fromDate,
        to_date: toDate,
      }),
      sources: await getData("analytics/sources", {
        from_date: fromDate,
        to_date: toDate,
      }),
    },
  };
}
