import { Cell } from "../components/Cell";
import ChartDataLabels from "chartjs-plugin-datalabels";
import Link from "next/link";

export default function Home({
  resourcesType,
  servicesType,
  resourcesCreationDate,
  resourcesTypeCreationDate,
  resourcesTypeIntegratedCreationDate,
  resourcesLanguage,
  resourcesProviders,
}) {
  return (
    <>
      <div className="title">
        <h1> ELG resources statistics</h1>
        <div className="links">
          <div className="link">
            <Link href="#resourcesType">
              <a>Types of the resources</a>
            </Link>
          </div>
          <div className="link">
            {" "}
            <Link href="#servicesType">
              <a>Types of the services</a>
            </Link>
          </div>
          <div className="link">
            {" "}
            <Link href="#resourcesProviders">
              <a>ELG and pilot projects</a>
            </Link>
          </div>
          <div className="link">
            {" "}
            <Link href="#resourcesCreationDate">
              <a>Number of resources</a>
            </Link>
          </div>
          <div className="link">
            {" "}
            <Link href="#resourcesTypeCreationDate">
              <a>Detail of the number of resources</a>
            </Link>
          </div>
          <div className="link">
            {" "}
            <Link href="#resourcesTypeIntegratedCreationDate">
              <a>Detail of the number of integrated resources</a>
            </Link>
          </div>
          <div className="link">
            {" "}
            <Link href="#resourcesLanguage">
              <a>Languages of resources</a>
            </Link>
          </div>
        </div>
      </div>
      <div>
        <Cell
          pie={true}
          chartHeight={30}
          id="resourcesType"
          title="Types of the resources"
          description=""
          small={true}
          data={resourcesType}
          plugins={[ChartDataLabels]}
          options={{
            animation: false,
          }}
        />
        <Cell
          bar={true}
          id="servicesType"
          title="Types of the services"
          description=""
          data={servicesType}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
            animation: false,
          }}
        />
        <Cell
          table={true}
          id="resourcesProviders"
          title="Resources provided by ELG or pilot projects"
          description="Number of resources per ELG partener and pilot project. For pilot projects it corresponds to the resources funded by the projects. For ELG partners, it corresponds to the resources provided by the organizations."
          data={resourcesProviders}
        />
        <Cell
          line={true}
          id="resourcesCreationDate"
          title="Number of resources in function of the time"
          description=""
          data={resourcesCreationDate}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
            animation: false,
          }}
        />
        <Cell
          line={true}
          id="resourcesTypeCreationDate"
          title="Detail of the number of resources in function of the time"
          description=""
          data={resourcesTypeCreationDate}
          options={{
            animation: false,
          }}
        />
        <Cell
          line={true}
          id="resourcesTypeIntegratedCreationDate"
          title="Detail of the number of resources integrated in ELG in function of the time"
          description="An integrated resource is a resource that has data hosted in ELG or a functional service."
          data={resourcesTypeIntegratedCreationDate}
          options={{
            animation: false,
          }}
        />
        <Cell
          bar={true}
          chartHeight={1000}
          id="resourcesLanguage"
          title="Languages of the resources"
          description=""
          tall={true}
          data={resourcesLanguage}
          options={{
            maintainAspectRatio: false,
            indexAxis: "y",
            animation: false,
            scales: {
              x: {
                stacked: true,
              },
              y: {
                stacked: true,
              },
            },
          }}
        />
      </div>
    </>
  );
}

async function getData(path, params = null) {
  function encode_params(params) {
    if (params == null) {
      return "";
    }
    var result = "?";
    for (var [k, vs] of Object.entries(params)) {
      if (typeof vs[Symbol.iterator] !== "function") {
        vs = [vs];
      }
      vs.forEach(function (v, _) {
        result += `${k}=${v}&`;
      });
    }
    return result.slice(0, -1);
  }
  const dataReq = await fetch(
    `${process.env.FASTAPI_URL}/${path}${encode_params(params)}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
  const data = await dataReq.json();
  return data;
}

export async function getServerSideProps() {
  return {
    props: {
      resourcesType: await getData("resources/type", {}),
      servicesType: await getData("resources/service_type", {
        types: ["ToolService"],
      }),
      resourcesLanguage: await getData("resources/languages", {
        to_1d: true,
        order_data: true,
        nb_of_colors: 1,
        one_dataset_per_type: true,
        types: [
          "Corpus",
          "LanguageDescription",
          "LexicalConceptualResource",
          "Organization",
          "Project",
          "ToolService",
        ],
      }),
      resourcesCreationDate: await getData("resources/creation_date", {
        cumulate: true,
        nb_of_colors: 1,
      }),
      resourcesTypeCreationDate: await getData("resources/creation_date", {
        cumulate: true,
        nb_of_colors: 1,
        one_dataset_per_type: true,
        types: [
          "Corpus",
          "LanguageDescription",
          "LexicalConceptualResource",
          "Organization",
          "Project",
          "ToolService",
        ],
      }),
      resourcesTypeIntegratedCreationDate: await getData(
        "resources/creation_date",
        {
          cumulate: true,
          nb_of_colors: 1,
          one_dataset_per_type: true,
          types: [
            "Corpus",
            "LanguageDescription",
            "LexicalConceptualResource",
            "ToolService",
          ],
          elg_integrated: true,
        }
      ),
      resourcesProviders: await getData("resources/provided_by", {
        one_dataset_per_type: true,
        types: [
          "Corpus",
          "LanguageDescription",
          "LexicalConceptualResource",
          "ToolService",
        ],
        total: true,
      }),
    },
  };
}
