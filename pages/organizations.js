import { Cell } from "../components/Cell";
import ChartDataLabels from "chartjs-plugin-datalabels";
import Link from "next/link";

export default function Home({
  organizationsLegalStatus,
  organizationsStartup,
  organizationsDivisionCategory,
  organizationsCountry,
}) {
  return (
    <>
      <div className="title">
        <h1> ELG organizations statistics</h1>
        <div className="links">
          <div className="link">
            <Link href="#organizationsLegalStatus">
              <a>Legal Status of the organizations</a>
            </Link>
          </div>
          {/* <div className="link">
            <Link href="#organizationsStartup">
              <a>Startups organizations</a>
            </Link>
          </div> */}
          <div className="link">
            <Link href="#organizationsDivisionCategory">
              <a>Division category of the organizations</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#organizationsCountryTable">
              <a>Type of organizations per country (Table)</a>
            </Link>
          </div>
          <div className="link">
            <Link href="#organizationsCountry">
              <a>Type of organizations per country</a>
            </Link>
          </div>
        </div>
      </div>
      <div>
        <Cell
          bar={true}
          chartHeight={30}
          id="organizationsLegalStatus"
          title="Legal Status of the organizations"
          description="Legal status of the organizations resources of the ELG catalogue."
          small={true}
          data={organizationsLegalStatus}
          plugins={[ChartDataLabels]}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
            animation: false,
          }}
        />
        {/* <Cell
          bar={true}
          chartHeight={30}
          id="organizationsStartup"
          title="Startups"
          description=""
          small={true}
          data={organizationsStartup}
          plugins={[ChartDataLabels]}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
            animation: false,
          }}
        /> */}
        <Cell
          bar={true}
          chartHeight={30}
          id="organizationsDivisionCategory"
          title="Division Category of the organizations"
          description="Each Academicinstitution has a division category field in the metadata record. A division is a sub-part of an academic institution. 'NotDivision' means that the academic institution is not a division but the parent institution."
          small={true}
          data={organizationsDivisionCategory}
          plugins={[ChartDataLabels]}
          options={{
            plugins: {
              legend: {
                display: false,
              },
            },
            animation: false,
          }}
        />
        <Cell
          table={true}
          chartHeight={60}
          id="organizationsCountryTable"
          title="Type of organizations per country (Table)"
          description="Detail of all organization types per country displayed in a table."
          data={organizationsCountry}
        />
        <Cell
          bar={true}
          chartHeight={60}
          id="organizationsCountry"
          title="Type of organizations per country"
          description="Detail of all organization types per country displayed as a bar chart."
          tall={true}
          data={organizationsCountry}
          options={{
            maintainAspectRatio: false,
            indexAxis: "y",
            animation: false,
            scales: {
              x: {
                stacked: true,
              },
              y: {
                stacked: true,
              },
            },
          }}
        />
      </div>
    </>
  );
}

async function getData(path, params = null) {
  function encode_params(params) {
    if (params == null) {
      return "";
    }
    var result = "?";
    for (var [k, vs] of Object.entries(params)) {
      if (typeof vs[Symbol.iterator] !== "function") {
        vs = [vs];
      }
      vs.forEach(function (v, _) {
        result += `${k}=${v}&`;
      });
    }
    return result.slice(0, -1);
  }
  const dataReq = await fetch(
    `${process.env.FASTAPI_URL}/${path}${encode_params(params)}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
  const data = await dataReq.json();
  return data;
}

export async function getServerSideProps() {
  return {
    props: {
      organizationsLegalStatus: await getData("resources/legal_status", {
        types: ["Organization"],
      }),
      organizationsStartup: await getData("resources/startup", {
        types: ["Organization"],
      }),
      organizationsDivisionCategory: await getData(
        "resources/division_category",
        {
          types: ["Organization"],
        }
      ),
      organizationsCountry: await getData("resources/country", {
        to_1d: true,
        order_data: true,
        nb_of_colors: 1,
        one_dataset_per_organization_type: true,
        types: ["Organization"],
        organization_types: [
          "Company",
          "Academicinstitution",
          "Academicinstitution_Branch",
          "Academicinstitution_Department",
          "Academicinstitution_Faculty",
          "Academicinstitution_Group",
          "Academicinstitution_Institute",
          "Academicinstitution_Laboratory",
          "Academicinstitution_School",
          "Academicinstitution_Subsidiary",
          "Academicinstitution_Unit1",
          "Association",
          "Largecompany",
          "Other",
          "Publicorganization",
          "Sme",
        ],
      }),
    },
  };
}
