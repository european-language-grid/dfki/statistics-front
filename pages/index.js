import Link from "next/link";
import React from "react";

const isProd = process.env.NODE_ENV === "production";
const linkBasePath = isProd ? "/statistics/" : "/";

export default function Home() {
  return (
    <>
      <div className="title">
        <h1> ELG statistics</h1>
        <Link href={`${linkBasePath}resources`}>
          <div className="menu">Resources stats</div>
        </Link>
        <Link href={`${linkBasePath}organizations`}>
          <div className="menu">Organizations stats</div>
        </Link>
        <Link href={`${linkBasePath}users`}>
          <div className="menu">Users stats</div>
        </Link>
        <Link href={`${linkBasePath}ga`}>
          <div className="menu">Google analytics stats</div>
        </Link>
        <div className="refreshDiv">
          <button
            onClick={async () => {
              alert(
                "The resources data will be updated in around 10min, the time to call the APIs."
              );
              await fetch(`${linkBasePath}api/refresh-resources`);
            }}
          >
            Refresh resources data
          </button>
          <button
            onClick={async () => {
              alert(
                "The users data will be updated in around 10min, the time to call the APIs."
              );
              await fetch(`${linkBasePath}api/refresh-users`);
            }}
          >
            Refresh users data
          </button>
          <button
            onClick={async () => {
              alert(
                "The login events data will be updated in around 10min, the time to call the APIs."
              );
              await fetch(`${linkBasePath}api/refresh-login-events`);
            }}
          >
            Refresh login events data
          </button>
        </div>
      </div>
    </>
  );
}
