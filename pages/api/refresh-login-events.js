export default async function handler(req, res) {
  fetch(`${process.env.FASTAPI_URL}/create/login-events`);
  res
    .status(200)
    .json({ url: `${process.env.FASTAPI_URL}/create/login-events` });
}
