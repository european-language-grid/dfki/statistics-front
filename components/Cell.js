import { Bar, Doughnut, Line, Pie, defaults } from "react-chartjs-2";
import { Table, TwoColumnsTable } from "./Table";

import Image from "next/image";
import { useRef } from "react";

// Disable animating charts by default.
defaults.animation = false;

export function download(filename, data) {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:application/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(data, null, 4))
  );
  element.setAttribute("download", filename);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

export function Cell(props) {
  const chart = useRef(null);
  return (
    <div id={props.id} className="container">
      <div className="titleChart">
        <h2>{props.title}</h2>
        <button
          onClick={function () {
            var data = props.data;
            var filename = "data.json";
            download(filename, data);
          }}
        >
          <Image
            src="/icons/download_black_24dp.svg"
            alt="Download data"
            width="24"
            height="24"
          />
        </button>
        {!(props.table || props.twoColumnsTable) && (
          <button
            onClick={function () {
              if (chart.current !== null) {
                var a = document.createElement("a");
                a.href = chart.current.toBase64Image();
                a.download = "chart.png";
                a.click();
              }
              console.log("table export is not supported yet.");
            }}
          >
            <Image
              src="/icons/launch_black_24dp.svg"
              alt="Download img"
              width="24"
              height="24"
            />
          </button>
        )}
      </div>
      <div className="descriptionChart">
        <p>{props.description}</p>
      </div>
      {props.bar && (
        <div
          style={{ height: `${props.chartHeight ? props.chartHeight : 50}rem` }}
          className="chartDiv"
        >
          <Bar
            ref={chart}
            data={props.data}
            plugins={props.plugins}
            options={props.options}
            className="chart"
          />
        </div>
      )}
      {props.doughnut && (
        <div
          style={{ height: `${props.chartHeight ? props.chartHeight : 30}rem` }}
          className="chartDiv"
        >
          <Doughnut
            ref={chart}
            data={props.data}
            plugins={props.plugins}
            options={props.options}
            className="chart"
          />
        </div>
      )}
      {props.line && (
        <div
          style={{ height: `${props.chartHeight ? props.chartHeight : 50}rem` }}
          className="chartDiv"
        >
          <Line
            ref={chart}
            data={props.data}
            plugins={props.plugins}
            options={props.options}
            className="chart"
          />
        </div>
      )}
      {props.pie && (
        <div
          style={{ height: `${props.chartHeight ? props.chartHeight : 30}rem` }}
          className="chartDiv"
        >
          <Pie
            ref={chart}
            data={props.data}
            plugins={props.plugins}
            options={props.options}
            className="chart"
          />
        </div>
      )}
      {props.table && <Table data={props.data} label={props.label} />}
      {props.twoColumnsTable && (
        <TwoColumnsTable data={props.data} label={props.label} />
      )}
    </div>
  );
}
