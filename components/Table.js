const zip = (...arrays) => {
  const minLen = Math.min(...arrays.map((arr) => arr.length));
  const [firstArr, ...restArrs] = arrays;
  return firstArr
    .slice(0, minLen)
    .map((val, i) => [val, ...restArrs.map((arr) => arr[i])]);
};

const addColum = (arrays) => {
  const newArrays = zip(
    ...[
      arrays.filter((element, index) => !(index % 2)),
      arrays.filter((element, index) => index % 2),
    ]
  ).map((a) => Array.prototype.concat(...a));
  return newArrays;
};

export function Table(props) {
  return (
    <table>
      <thead>
        <tr>
          <td>{props.label}</td>
          {props.data.datasets
            .map((v) => v.label)
            .map((value) => (
              <td>{value}</td>
            ))}
        </tr>
      </thead>
      <tbody>
        {zip(
          ...[props.data.labels].concat(props.data.datasets.map((v) => v.data))
        ).map((values) => (
          <tr>
            {values.map((value) => (
              <td>{isNaN(value) ? value : Math.round(value)}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
}
export function TwoColumnsTable(props) {
  return (
    <table>
      <thead>
        <tr>
          <td>{props.label}</td>
          {props.data.datasets
            .map((v) => v.label)
            .map((value) => (
              <td>{value}</td>
            ))}
          <td>{props.label}</td>
          {props.data.datasets
            .map((v) => v.label)
            .map((value) => (
              <td>{value}</td>
            ))}
        </tr>
      </thead>
      <tbody>
        {addColum(
          zip(
            ...[props.data.labels].concat(
              props.data.datasets.map((v) => v.data)
            )
          )
        ).map((values) => (
          <tr>
            {values.map((value) => (
              <td>{isNaN(value) ? value : Math.round(value)}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
}
